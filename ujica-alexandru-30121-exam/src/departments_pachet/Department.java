package departments_pachet;

import entities_pachet.Employee;

public abstract class Department {
    private int id;
    private String name;

    abstract public void addEmployee(Employee e);
    abstract public void removeEmployee(Employee e);
    abstract public Employee getEmployee(int id);
    abstract public void updateEmployee(Employee e);
}
