package entities_pachet;

import departments_pachet.Department;
import departments_pachet.Finance;
import departments_pachet.IT;

import java.util.ArrayList;

public class Company {
    ArrayList<Department>departments=new ArrayList<>();
    private String name;
    private String adress;
    public Company(String name, String adrees){
        this.name=name;
        this.adress=adrees;
    }
    public void initDepartments(){
        Department a1=new IT();
        Department a2=new Finance();
        departments.add(a1);
        departments.add(a2);
    }

    @Override
    public String toString() {
        return "Company{" +
                "departments=" + departments +
                ", name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                '}';
    }
}

