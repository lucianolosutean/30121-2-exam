package entities_pachet;

import actions_pachet.LunchBreak;
import actions_pachet.WorkDay;

public abstract class  Employee extends Person {
    private boolean working;
    private int departmentId;

    abstract public void work();
    abstract public void relax();
}
