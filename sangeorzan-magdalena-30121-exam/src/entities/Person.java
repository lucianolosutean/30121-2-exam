package entities;

public class Person {

    private int id;
    private String name;
    private String address;


    public boolean equals(Object obj) {
        if(obj instanceof Person){
            Person p = (Person)obj;
            return id == p.id;
        }
        return false;
    }
}
