package departments;

import entities.Accountant;
import entities.Employee;

import java.util.ArrayList;

public class Finance extends Department{
    //
    private ArrayList<Object> accountants = new ArrayList<>();

    //methods
    public void addEmployee(Employee e) {
        accountants.add(e);
    }

    public void removeEmployee(Employee e) {
        if(accountants.isEmpty()) System.out.println("No employees to be removed!");
        else accountants.remove(e);
    }

    public Employee getEmployee(int id) {

        try {
            return (Employee)accountants.get(id);
        }
        catch (Exception e)
        {
            System.out.println("Employee not found! The id was: "+id);
            return null;
        }

    }

    public void updateEmployee(Employee e)
    {
        if(accountants.contains(e))
        {
            for(Object o:accountants)
            {

            }
        }
    }

}
