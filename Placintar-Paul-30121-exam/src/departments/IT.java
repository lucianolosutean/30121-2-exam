package departments;

import entities.Employee;
import java.util.ArrayList;

public class IT extends Department {
    private ArrayList<Object> programmers = new ArrayList<>();

    //methods
    public void addEmployee(Employee e) {
        programmers.add(e);
    }

    public void removeEmployee(Employee e) {
        if(programmers.isEmpty()) System.out.println("No employees to be removed!");
        else programmers.remove(e);
    }

    public Employee getEmployee(int id) {

        try {
            return (Employee)programmers.get(id);
        }
        catch (Exception e)
        {
            System.out.println("Employee not found! The id was: "+id);
            return null;
        }

    }

    public void updateEmployee(Employee e)
    {
        System.out.println("Update IT");
    }
}
