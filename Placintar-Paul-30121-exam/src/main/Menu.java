package main;

import departments.Finance;
import entities.Company;
import entities.Employee;
import entities.Person;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class Menu {
    public JPanel menyPanel;
    private JLabel CompanyLabel;
    private JTextArea ITArea;
    private JTextArea FinanceArea;
    private JButton Add;
    private JButton Delete;
    private JButton Edit;
    private JButton MakeWork;
    private JButton MakeRelax;
    private JTextArea Console;
    public static int focus;

    public Menu() {
        Add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(focus==1)
                {
                   //Employee e1 = new Person(1,"Paul","adresa1");
                }
                Console.setText(""+focus);
            }
        });
        ITArea.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                focus = 2;
            }
        });
        FinanceArea.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                focus = 1;
            }
        });
    }

    //init the menuForm
    public void init()
    {
        Company c = new Company("TestCompany","Str. Oberservatorului nr 5");
        CompanyLabel.setText(c.getName());
        c.initDepartments();
        focus = 0;
    }

}
