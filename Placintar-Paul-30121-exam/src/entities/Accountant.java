package entities;

import actions.*;


public class Accountant extends Employee implements WorkDay, LunchBreak  {

    @Override
    public void work()
    {
        System.out.println("Accountant working");
    }

    @Override
    public void relax() {
        System.out.println("Accountant relaxing");
    }
}
