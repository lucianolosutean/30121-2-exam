package entities;

import departments.Department;
import departments.Finance;
import departments.IT;

public class Company {
    private String name;
    private String address;
    private Department d1;
    private Department d2;

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public void initDepartments()
    {
        System.out.println("Init departments");
        d1 = new IT();
        d2 = new Finance();
        System.out.println("Init departments done");
    }

    public String getName() {
        return name;
    }

    public Department getD1() {
        return d1;
    }

    public Department getD2() {
        return d2;
    }
}
