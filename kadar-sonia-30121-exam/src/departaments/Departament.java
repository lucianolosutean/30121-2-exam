package departaments;
import entities.*;
public abstract class Departament {
	int id;
	String name;
	public abstract void addEmployee(Employee e);
	public abstract void removeEmployee(Employee e);
	public abstract void updateEmployee(Employee e);
	public abstract Employee getEmployee(int id);

}
