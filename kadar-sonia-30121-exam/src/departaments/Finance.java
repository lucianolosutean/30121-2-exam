package departaments;

import java.util.ArrayList;
import entities.*;

public class Finance extends Departament{
	private ArrayList<Accountant> accountants = new ArrayList();
	
	public  void addEmployee(Employee e){
	    accountants.add( (Accountant) e);	
	}
	public void removeEmployee(Employee e){
		accountants.remove( (Accountant) e);
	}
	public void updateEmployee(Employee e){
		
	}
	public Employee getEmployee(int id){ 
		try {
            return (Employee) accountants.get(id);
        }
        catch (Exception e)
        {
            System.out.println("Employee not found.");
            return null;
        }
	}

}
