package departaments;

import java.util.ArrayList;

import entities.Employee;
import entities.Programmer;

public class IT extends Departament{
	private ArrayList<Programmer> programmers = new ArrayList();
	
	public  void addEmployee(Employee e){
		programmers.add( (Programmer) e);	
	}
	public void removeEmployee(Employee e){		
		programmers.remove( (Programmer) e);
	}
	public void updateEmployee(Employee e){
		
	}
	public Employee getEmployee(int id){ 
		try {
            return (Employee) programmers.get(id);
        }
        catch (Exception e)
        {
            System.out.println("Employee not found.");
            return null;
        }
	}
}
