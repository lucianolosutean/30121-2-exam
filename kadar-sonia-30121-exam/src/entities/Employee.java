package entities;
import actions.*;
public abstract class Employee extends Person implements WorkDay, LunchBreak {
  private boolean working;
  private int departamentId;
  
  public abstract void work();
  public abstract void relax();
  
  public void setWorking(boolean working){
	  this.working = working;
  }
  
  public boolean getWorking(){
	  return working;
  }
}
