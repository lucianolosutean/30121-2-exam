package entities;
import java.util.ArrayList;
import departaments.*;
public class Company {
	private String name;
	private String address;
	private ArrayList<Departament> departaments;
	
	public Company(String name, String address){
		this.name=name;
		this.address=address;
	}
	public void initDepartament(){
		departaments = new ArrayList();
	}

	public ArrayList<Departament> getDepartaments() {
		return departaments;
	}
	public void setDepartaments(ArrayList<Departament> departaments) {
		this.departaments = departaments;
	}
}
