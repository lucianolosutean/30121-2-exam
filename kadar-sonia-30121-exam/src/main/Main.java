package main;
import departaments.*;
import entities.*;
import java.util.ArrayList;
public class Main {

	public static void main(String[] args) {
		Accountant a1 = new Accountant();
		Accountant a2 = new Accountant();
		Accountant a3 = new Accountant();
		Finance f1 = new Finance();
		f1.addEmployee(a1);
		f1.addEmployee(a2);
		f1.addEmployee(a3);
		a1.work();
		a2.relax();
		a3.work();
		Programmer p1 = new Programmer();
		Programmer p2 = new Programmer();
		Programmer p3 = new Programmer();
		p1.work();
		p2.relax();
		p3.relax();
		IT it = new IT();
		it.addEmployee(p1);
		it.addEmployee(p2);
		it.addEmployee(p3);
		
		
		Company c = new Company("Company1", "Address1");
		c.initDepartament();
		ArrayList<Departament> departaments = new ArrayList();
		departaments.add(f1);
		departaments.add(it);
		
		c.setDepartaments(departaments);
	    
	}

}
