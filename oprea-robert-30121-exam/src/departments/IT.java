package departments;

import entities.Employee;

import java.util.ArrayList;

public class IT {
    private ArrayList<Object> programmer = new ArrayList<>();

    public void addEmployee(Employee e) {
        programmer.add(e);
    }

    public void removeEmployee(Employee e) {
        if (programmer.isEmpty()) System.out.println("Programmer not found");
        else programmer.remove(e);
    }

    public Employee getEmployee(int id) {
        try {
            programmer.get(id);
        } catch (Exception e) {
            System.out.println("Error! Programmer " + id + "not found");
            return null;
        }
        return null;
    }

    public Employee updateEmployee(Employee e) {

        return e;
    }
}
