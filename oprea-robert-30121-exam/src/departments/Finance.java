package departments;

import entities.Employee;

import java.util.ArrayList;

public class Finance extends Department {

    private ArrayList<Object> accountants = new ArrayList<>();

    public void addEmployee(Employee e) {
        accountants.add(e);
    }

    public void removeEmployee(Employee e) {
        if (accountants.isEmpty()) System.out.println("Employee not found");
        else accountants.remove(e);
    }

    public Employee getEmployee (int id) {
        try {
            accountants.get(id);
        } catch (Exception e) {
            System.out.println("Error! Employee " + id + "not found");
            return null;
        }

        return null;
    }

    public Employee updateEmployee(Employee e) {
        System.out.println("Updating employee info");

        return e;
    }
}
