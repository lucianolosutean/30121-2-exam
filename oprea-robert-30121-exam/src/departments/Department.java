package departments;

import entities.Employee;

public abstract class Department {
    private int id;
    private String name;

    public abstract void addEmployee(Employee e);

    public abstract void removeEmployee(Employee e);

    public abstract Employee getEmployee(int id);

    public abstract Employee updateEmployee(Employee e);
}
