package entities;

public abstract class Employee extends Person {
    private boolean working;
    private int departmentId;

    public abstract void work();
    public abstract void relax();
}
