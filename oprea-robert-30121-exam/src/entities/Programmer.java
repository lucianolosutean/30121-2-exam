package entities;

import actions.*;

public class Programmer extends Employee implements WorkDay, LunchBreak {

    @Override
    public void relax(){
        System.out.println("Programmer is relaxing");
    }

    @Override
    public void work(){
        System.out.println("Programmer is working");
    }
}
