package entities;

import actions.*;

public class Accountant extends Employee implements WorkDay,LunchBreak {

    @Override
    public void work(){
        System.out.println("Accountant is working");
    }

    @Override
    public void relax(){
        System.out.println("Accountant is relaxing");
    }
}
