package departments;

import entities.Employee;
import entities.Programmer;

import java.util.ArrayList;

public class IT extends Department {

    ArrayList<Programmer> ITemployees = new ArrayList<Programmer>();

    public void addEmployee(Employee e) {
        Programmer p = (Programmer) e;
        ITemployees.add(p);
    }

    public void removeEmployee(Employee e) {
        Programmer p = (Programmer) e;
        ITemployees.remove(p);
    }

    public void updateEmployee(Employee e) {
        Employee e1=getEmployee(e.getId());
        e1.setName(e.getName());
        e1.setAddress(e.getAddress());
        e1.setWorking(e.isWorking());
        e1.setDepartmentId(e.getDepartmentId());

        //updateaza angajatul cu acelasi id modificand celelalte argumente :-??
    }

    public Employee getEmployee(int id) {
        Programmer p=new Programmer();
        for (int i = 0; i < ITemployees.size(); i++) {
            if (ITemployees.get(i).getId()==id) {
                p = ITemployees.get(i);
                break;
            }
        }
        Employee e=(Employee)p;
        return e;
    }

}
