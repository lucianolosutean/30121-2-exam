package departments;

import entities.Accountant;
import entities.Employee;
import entities.Programmer;

import java.util.ArrayList;

public class Finance extends Department {

    ArrayList<Accountant> Femployees = new ArrayList<Accountant>();

    public void addEmployee(Employee e) {
        Accountant p = (Accountant) e;
        Femployees.add(p);
    }

    public void removeEmployee(Employee e) {
        Accountant p = (Accountant) e;
        Femployees.remove(p);
    }

    public void updateEmployee(Employee e) {
        Employee e1=getEmployee(e.getId());
        e1.setName(e.getName());
        e1.setAddress(e.getAddress());
        e1.setWorking(e.isWorking());
        e1.setDepartmentId(e.getDepartmentId());

        //updateaza angajatul cu acelasi id modificand celelalte argumente :-??
    }

    public Employee getEmployee(int id) {
        Accountant p=new Accountant();
        for (int i = 0; i < Femployees.size(); i++) {
            if (Femployees.get(i).getId()==id) {
                p = Femployees.get(i);
                break;
            }
        }
        Employee e=(Employee)p;
        return e;
    }
}
