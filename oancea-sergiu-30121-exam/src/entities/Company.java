package entities;

import departments.Finance;
import departments.IT;

public class Company {
    private String name;
    private String address;

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public void initDepartments(){
        IT ITDepartment=new IT();
        Finance FinanceDepartment=new Finance();
    }
}
