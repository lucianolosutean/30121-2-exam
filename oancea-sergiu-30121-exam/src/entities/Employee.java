package entities;

abstract public class Employee extends Person{
    private boolean working;
    private int departmentId;

    abstract void work();

    abstract void relax();

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }


}
