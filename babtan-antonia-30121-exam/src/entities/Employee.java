package entities;

public abstract class Employee extends Person{

    private boolean working;
    private int departmentId;


    public Employee(int id, String name, String address) {
        super(id, name, address);
    }

    public Employee(){
        this.working=true;
        this.departmentId=1;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    abstract void work();
    abstract void relax();

    public Employee(boolean working, int departmentId) {
        this.working = working;
        this.departmentId = departmentId;
    }




}
