package Entities;
import Actions.LunchDay;
import Actions.WorkDay
abstract class  Employee extends Person implements WorkDay,LunchDay {
    private boolean working;
    private int departmentId;

    public Employee(boolean working, int departmentId, int id, String name, String address) {
        super(id, name, address);
        this.working = working;
        this.departmentId = departmentId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public boolean getWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;

    }


    abstract public void work(){

    }

    abstract public void relax(){}}


