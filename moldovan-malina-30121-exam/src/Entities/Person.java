package Entities;

public class Person {
    private int id;
    private String name;
    private String address;
     public Person(int id, String address, String name){
         this.id=id;
         this.name=name;
         this.address=address

     }
     public int getId(){
         return id;
     }
     public void setId(int id){
         this.id=id;
     }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getAddress(){
        return address;
    }
    public void setAddress(String name) {
        this.address = address;
    }
}
