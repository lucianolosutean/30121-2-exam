package entities;

import departments.Finance;

import java.util.ArrayList;

public class Accountant extends Employee {
    ArrayList<Finance> finances= new ArrayList<Finance>();

    @Override
    void work() {
        System.out.println("Acest contabil munceste.");
    }

    @Override
    void relax() {
        System.out.println("Acest contabil e in pauza.");
    }
}
