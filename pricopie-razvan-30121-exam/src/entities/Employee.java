package entities;

abstract public class Employee extends Person {
    private boolean working;
    private int departmentID;

    public Employee(int id, String name, String adress, boolean working, int departmentID) {
        super(id, name, adress);
        this.working = working;
        this.departmentID = departmentID;
    }

    abstract void work();
   abstract void relax();

}
