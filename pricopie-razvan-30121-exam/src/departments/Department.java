package departments;

import entities.Company;
import entities.Employee;

import java.util.ArrayList;

abstract public class Department {
    private int id;
    private String name;
    public Employee e=new Employee();
    private ArrayList<Company> companies=new ArrayList<Company>();

    abstract void addEmployee(Employee e);
    abstract void removeEmployee(Employee e);
    abstract void updateEmployee(Employee e);
    abstract Employee getEmployee(int id);
}
