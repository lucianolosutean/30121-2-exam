package entities;


abstract class Employee extends Person {
    private boolean working;
    private int depatament ;

    abstract void work();
    abstract void relax();

    public boolean isWorking() {
        return working;
    }

    public int getDepatament() {
        return depatament;
    }
}
