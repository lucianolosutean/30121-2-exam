package departments;

import entities.Employee;

public abstract class Department {
    public int id;
    public String name;

    abstract void addEmployee(Employee e);
    abstract void removeEmployee(Employee e);
    abstract void updateEmployee(Employee e);
    //abstract Employee getEmployee(int id);
}
