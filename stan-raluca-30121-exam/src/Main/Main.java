package Main;

import departments.Department;
import departments.Finance;
import departments.IT;
import entities.Employee;

import java.io.*;

public class Main {



    private static Object e;

    public static void main(String[] args) throws IOException {
        try {

            Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(data.txt)));
            Writer writer=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(data.txt)));
            Department It = new Department(departments.IT);
            Department Finance = new Department(departments.Finance);
            Employee e1 = new Employee(2, "Popescu", "Bacau");
            Employee e2 = new Employee(3, "Marinescu", "Cismigiu");


            String op = args[0];
            String actiune = args[1];
            switch (op) {

                case 1: {
                    actiune = "addEmployee";
                    It.addEmployee(e);
                    Finance.addEmployee(e);
                }
                break;

                case 2: {

                    actiune = "removeEmployee";
                    It.removeEmployee(e1);
                    Finance.removeEmployee(e2);
                }
                break;
                case 3: {

                    actiune = "updateEmployee";
                    It.updateEmployee(e1);
                    Finance.removeEmployee(e1);
                }
                break;
                case 4: {

                    actiune = "workEmployee";
                    e1.work();
                    e2.work();

                }
                break;
                case 5: {

                    actiune = "breakEmployee";
                    e1.relax();
                    e2.relax();
                }
                break;
                default:
                    System.out.println("Nu s-a selectat nicio optiune");

            }


         catch(IOException e){
                System.out.println("Nu s-a deschis fisierul");
            }

          catch(ArrayIndexOutOfBoundsException e){

                System.out.println("Index Out of Bounds");
            }
        }
    }




